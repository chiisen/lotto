﻿using NLog;
using System;
using System.Windows.Forms;

namespace Lotto
{
    static class Program
    {
        private static Logger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        [STAThread]
        static void Main()
        {
            LogManager.Configuration.Variables["MY_DATE"] = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

            log.Debug("=======");
            log.Debug("程式啟動");
            log.Debug("=======");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }
    }
}
