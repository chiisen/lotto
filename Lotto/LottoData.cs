﻿using System;
using System.Globalization;

namespace Lotto
{
    /// <summary>
    //? 儲存樂透資料
    /// </summary>
    public class LottoData
    {
        public long lDate = 0;
        public DateTime cDate = DateTime.Now;

        public string TaiwanDate = "None";//民國
        public int Number1 = 0;
        public int Number2 = 0;
        public int Number3 = 0;
        public int Number4 = 0;
        public int Number5 = 0;
        public int Number6 = 0;
        public int SpecialNumber = 0;

        public LottoData(string date, object[] obj)
        {
            TaiwanDate = date;
            cDate = TaiwanCalendar(date);
            lDate = DateTimeToLong();

            Number1 = int.Parse(obj[1].ToString());
            Number2 = int.Parse(obj[2].ToString());
            Number3 = int.Parse(obj[3].ToString());
            Number4 = int.Parse(obj[4].ToString());
            Number5 = int.Parse(obj[5].ToString());
            Number6 = int.Parse(obj[6].ToString());

            SpecialNumber = int.Parse(obj[7].ToString());
        }

        /// <summary>
        /// 民國轉西元
        /// </summary>
        /// <param name="sampleDate"></param>
        /// <returns></returns>
        private DateTime TaiwanCalendar(string sampleDate)
        {
            CultureInfo culture = new CultureInfo("zh-TW");
            culture.DateTimeFormat.Calendar = new TaiwanCalendar();
            return DateTime.Parse(sampleDate, culture);
        }

        /// <summary>
        /// 轉為數字字串列
        /// </summary>
        /// <returns></returns>
        public string NumbersLine()
        {
            return Number1.ToString("D2") + ","
                 + Number2.ToString("D2") + ","
                 + Number3.ToString("D2") + ","
                 + Number4.ToString("D2") + ","
                 + Number5.ToString("D2") + ","
                 + Number6.ToString("D2");
        }

        /// <summary>
        /// 時間傳為數值 for 快速排序
        /// </summary>
        /// <returns></returns>
        private long DateTimeToLong()
        {
            return cDate.Ticks;
        }

        /// <summary>
        /// 數字是否包含指定號碼
        /// </summary>
        /// <param name="TargetNumber"></param>
        /// <returns></returns>
        public bool Include(int TargetNumber)
        {
            if (TargetNumber == Number1 ||
                TargetNumber == Number2 ||
                TargetNumber == Number3 ||
                TargetNumber == Number4 ||
                TargetNumber == Number5 ||
                TargetNumber == Number6 ||
                TargetNumber == SpecialNumber)
            {
                return true;
            }
            return false;
        }
    };
}
