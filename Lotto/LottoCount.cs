﻿using NLog;
using System.Collections.Generic;
using System.Linq;

namespace Lotto
{
    /// <summary>
    //? 儲存樂透統計資料
    /// </summary>
    public class LottoCount
    {
        private Logger log = LogManager.GetCurrentClassLogger();

        public int TargetNumber = 0;// 要預測的號碼
        public LottoData CacheNumber = null;// 暫存下一期

        public bool First = true;

        public Dictionary<int, int> TotalCount = new Dictionary<int, int>();

        public LottoCount(int num)
        {
            TargetNumber = num;
        }

        /// <summary>
        /// 更新下一期號碼
        /// </summary>
        /// <param name="lotto"></param>
        private void UpdateNextNumber(LottoData lotto)
        {
            CacheNumber = lotto;
        }

        public void Check(LottoData lotto)
        {
            if (CacheNumber == null)
            {
                //! 沒有下一期資料，紀錄一期號碼並結束

                // 更新下一期號碼
                UpdateNextNumber(lotto);

                log.Debug(string.Format("沒有下一期資料:　{0} -  {1} {2} {3} {4} {5} {6} {7}", lotto.TaiwanDate, lotto.Number1, lotto.Number2, lotto.Number3, lotto.Number4, lotto.Number5, lotto.Number6, lotto.SpecialNumber));
                return;
            }

            if (lotto.Include(TargetNumber))
            {
                // 這期有在裡面

                // 累計下一期資料
                int i = 1;
                List<int> cacheNumers_ = new List<int>()
                {
                    CacheNumber.Number1,
                    CacheNumber.Number2,
                    CacheNumber.Number3,
                    CacheNumber.Number4,
                    CacheNumber.Number5,
                    CacheNumber.Number6,
                    CacheNumber.SpecialNumber
                };
                foreach (var n in cacheNumers_)
                {
                    log.Debug(string.Format("{0} - 如果開出號碼為 {1} 預測號碼就為{2} 預測來源={3}", i, TargetNumber, n, CacheNumber.TaiwanDate));
                    
                    UpdateTotalCount(n);

                    i += 1;
                }
                log.Debug("==================================================================================");
            }

            // 更新下一期號碼
            UpdateNextNumber(lotto);

            log.Debug(string.Format("更新下一期號碼:　{0} - {1} {2} {3} {4} {5} {6} {7}", lotto.TaiwanDate, lotto.Number1, lotto.Number2, lotto.Number3, lotto.Number4, lotto.Number5, lotto.Number6, lotto.SpecialNumber));
        }

        /// <summary>
        /// 更新統計資料
        /// </summary>
        /// <param name="n"></param>
        private void UpdateTotalCount(int n)
        {
            if (TotalCount.ContainsKey(n))
            {
                TotalCount[n] += 1;
            }
            else
            {
                TotalCount.Add(n, 1);
            }
        }

        /// <summary>
        /// 轉為數字字串列
        /// </summary>
        /// <returns></returns>
        public string NumbersLine()
        {
            // ascending 小到大
            // descending 大到小
            var dicSort = from objDic in TotalCount orderby objDic.Key ascending select objDic;

            string line_ = "";
            foreach (KeyValuePair<int, int> kvp in dicSort)
            {
                line_ += kvp.Key.ToString("D2") + ",";
            }

            line_ += "\n";

            foreach (KeyValuePair<int, int> kvp in dicSort)
            {
                line_ += kvp.Value.ToString("D2") + ",";
            }

            return line_;
        }
    }
}
