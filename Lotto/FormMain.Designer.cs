﻿
namespace Lotto
{
    partial class FormMain
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageFind = new System.Windows.Forms.TabPage();
            this.labelSpecialNumberInfo = new System.Windows.Forms.Label();
            this.labelNumbersInfo = new System.Windows.Forms.Label();
            this.labelDateInfo = new System.Windows.Forms.Label();
            this.labelSpecialNumber = new System.Windows.Forms.Label();
            this.labelNumbers = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.tabPageAdd = new System.Windows.Forms.TabPage();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.textBoxSpecialNumber = new System.Windows.Forms.TextBox();
            this.textBoxNumber6 = new System.Windows.Forms.TextBox();
            this.textBoxNumber5 = new System.Windows.Forms.TextBox();
            this.textBoxNumber4 = new System.Windows.Forms.TextBox();
            this.textBoxNumber3 = new System.Windows.Forms.TextBox();
            this.textBoxNumber2 = new System.Windows.Forms.TextBox();
            this.textBoxNumber1 = new System.Windows.Forms.TextBox();
            this.textBoxDate = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPageDel = new System.Windows.Forms.TabPage();
            this.buttonDel = new System.Windows.Forms.Button();
            this.textBoxDel = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPagePredict = new System.Windows.Forms.TabPage();
            this.textBoxPredictSpecialNumber1 = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber16 = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber15 = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber14 = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber13 = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber12 = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber11 = new System.Windows.Forms.TextBox();
            this.labelPredictSpecialNumber1 = new System.Windows.Forms.Label();
            this.labelPredictNumber16 = new System.Windows.Forms.Label();
            this.labelPredictNumber15 = new System.Windows.Forms.Label();
            this.labelPredictNumber14 = new System.Windows.Forms.Label();
            this.labelPredictNumber13 = new System.Windows.Forms.Label();
            this.labelPredictNumber12 = new System.Windows.Forms.Label();
            this.labelPredictNumber11 = new System.Windows.Forms.Label();
            this.buttonPredict = new System.Windows.Forms.Button();
            this.textBoxPredictSpecialNumber = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber6 = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber5 = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber4 = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber3 = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber2 = new System.Windows.Forms.TextBox();
            this.textBoxPredictNumber1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControlMain.SuspendLayout();
            this.tabPageFind.SuspendLayout();
            this.tabPageAdd.SuspendLayout();
            this.tabPageDel.SuspendLayout();
            this.tabPagePredict.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPageFind);
            this.tabControlMain.Controls.Add(this.tabPageAdd);
            this.tabControlMain.Controls.Add(this.tabPageDel);
            this.tabControlMain.Controls.Add(this.tabPagePredict);
            this.tabControlMain.Location = new System.Drawing.Point(12, 12);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(776, 426);
            this.tabControlMain.TabIndex = 0;
            // 
            // tabPageFind
            // 
            this.tabPageFind.Controls.Add(this.labelSpecialNumberInfo);
            this.tabPageFind.Controls.Add(this.labelNumbersInfo);
            this.tabPageFind.Controls.Add(this.labelDateInfo);
            this.tabPageFind.Controls.Add(this.labelSpecialNumber);
            this.tabPageFind.Controls.Add(this.labelNumbers);
            this.tabPageFind.Controls.Add(this.labelDate);
            this.tabPageFind.Location = new System.Drawing.Point(4, 25);
            this.tabPageFind.Name = "tabPageFind";
            this.tabPageFind.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFind.Size = new System.Drawing.Size(768, 397);
            this.tabPageFind.TabIndex = 0;
            this.tabPageFind.Text = "查詢";
            this.tabPageFind.UseVisualStyleBackColor = true;
            // 
            // labelSpecialNumberInfo
            // 
            this.labelSpecialNumberInfo.AutoSize = true;
            this.labelSpecialNumberInfo.BackColor = System.Drawing.Color.LightYellow;
            this.labelSpecialNumberInfo.Location = new System.Drawing.Point(230, 44);
            this.labelSpecialNumberInfo.Name = "labelSpecialNumberInfo";
            this.labelSpecialNumberInfo.Size = new System.Drawing.Size(24, 17);
            this.labelSpecialNumberInfo.TabIndex = 5;
            this.labelSpecialNumberInfo.Text = "08";
            // 
            // labelNumbersInfo
            // 
            this.labelNumbersInfo.AutoSize = true;
            this.labelNumbersInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.labelNumbersInfo.Location = new System.Drawing.Point(95, 44);
            this.labelNumbersInfo.Name = "labelNumbersInfo";
            this.labelNumbersInfo.Size = new System.Drawing.Size(124, 17);
            this.labelNumbersInfo.TabIndex = 4;
            this.labelNumbersInfo.Text = "03,30,06,11,17,14";
            // 
            // labelDateInfo
            // 
            this.labelDateInfo.AutoSize = true;
            this.labelDateInfo.BackColor = System.Drawing.Color.LightYellow;
            this.labelDateInfo.Location = new System.Drawing.Point(16, 44);
            this.labelDateInfo.Name = "labelDateInfo";
            this.labelDateInfo.Size = new System.Drawing.Size(72, 17);
            this.labelDateInfo.TabIndex = 3;
            this.labelDateInfo.Text = "110/08/30";
            // 
            // labelSpecialNumber
            // 
            this.labelSpecialNumber.AutoSize = true;
            this.labelSpecialNumber.Location = new System.Drawing.Point(229, 13);
            this.labelSpecialNumber.Name = "labelSpecialNumber";
            this.labelSpecialNumber.Size = new System.Drawing.Size(50, 17);
            this.labelSpecialNumber.TabIndex = 2;
            this.labelSpecialNumber.Text = "第二區";
            // 
            // labelNumbers
            // 
            this.labelNumbers.AutoSize = true;
            this.labelNumbers.Location = new System.Drawing.Point(95, 13);
            this.labelNumbers.Name = "labelNumbers";
            this.labelNumbers.Size = new System.Drawing.Size(50, 17);
            this.labelNumbers.TabIndex = 1;
            this.labelNumbers.Text = "第一區";
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Location = new System.Drawing.Point(16, 13);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(50, 17);
            this.labelDate.TabIndex = 0;
            this.labelDate.Text = "開獎日";
            // 
            // tabPageAdd
            // 
            this.tabPageAdd.Controls.Add(this.buttonAdd);
            this.tabPageAdd.Controls.Add(this.textBoxSpecialNumber);
            this.tabPageAdd.Controls.Add(this.textBoxNumber6);
            this.tabPageAdd.Controls.Add(this.textBoxNumber5);
            this.tabPageAdd.Controls.Add(this.textBoxNumber4);
            this.tabPageAdd.Controls.Add(this.textBoxNumber3);
            this.tabPageAdd.Controls.Add(this.textBoxNumber2);
            this.tabPageAdd.Controls.Add(this.textBoxNumber1);
            this.tabPageAdd.Controls.Add(this.textBoxDate);
            this.tabPageAdd.Controls.Add(this.label1);
            this.tabPageAdd.Controls.Add(this.label3);
            this.tabPageAdd.Controls.Add(this.label4);
            this.tabPageAdd.Location = new System.Drawing.Point(4, 25);
            this.tabPageAdd.Name = "tabPageAdd";
            this.tabPageAdd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAdd.Size = new System.Drawing.Size(768, 397);
            this.tabPageAdd.TabIndex = 1;
            this.tabPageAdd.Text = "新增";
            this.tabPageAdd.UseVisualStyleBackColor = true;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(25, 340);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(110, 41);
            this.buttonAdd.TabIndex = 14;
            this.buttonAdd.Text = "新增";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // textBoxSpecialNumber
            // 
            this.textBoxSpecialNumber.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxSpecialNumber.Location = new System.Drawing.Point(343, 38);
            this.textBoxSpecialNumber.Name = "textBoxSpecialNumber";
            this.textBoxSpecialNumber.Size = new System.Drawing.Size(30, 22);
            this.textBoxSpecialNumber.TabIndex = 13;
            // 
            // textBoxNumber6
            // 
            this.textBoxNumber6.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxNumber6.Location = new System.Drawing.Point(289, 38);
            this.textBoxNumber6.Name = "textBoxNumber6";
            this.textBoxNumber6.Size = new System.Drawing.Size(30, 22);
            this.textBoxNumber6.TabIndex = 12;
            // 
            // textBoxNumber5
            // 
            this.textBoxNumber5.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxNumber5.Location = new System.Drawing.Point(253, 38);
            this.textBoxNumber5.Name = "textBoxNumber5";
            this.textBoxNumber5.Size = new System.Drawing.Size(30, 22);
            this.textBoxNumber5.TabIndex = 11;
            // 
            // textBoxNumber4
            // 
            this.textBoxNumber4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxNumber4.Location = new System.Drawing.Point(214, 38);
            this.textBoxNumber4.Name = "textBoxNumber4";
            this.textBoxNumber4.Size = new System.Drawing.Size(30, 22);
            this.textBoxNumber4.TabIndex = 10;
            // 
            // textBoxNumber3
            // 
            this.textBoxNumber3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxNumber3.Location = new System.Drawing.Point(178, 38);
            this.textBoxNumber3.Name = "textBoxNumber3";
            this.textBoxNumber3.Size = new System.Drawing.Size(30, 22);
            this.textBoxNumber3.TabIndex = 9;
            // 
            // textBoxNumber2
            // 
            this.textBoxNumber2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxNumber2.Location = new System.Drawing.Point(142, 38);
            this.textBoxNumber2.Name = "textBoxNumber2";
            this.textBoxNumber2.Size = new System.Drawing.Size(30, 22);
            this.textBoxNumber2.TabIndex = 8;
            // 
            // textBoxNumber1
            // 
            this.textBoxNumber1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBoxNumber1.Location = new System.Drawing.Point(106, 38);
            this.textBoxNumber1.Name = "textBoxNumber1";
            this.textBoxNumber1.Size = new System.Drawing.Size(30, 22);
            this.textBoxNumber1.TabIndex = 7;
            // 
            // textBoxDate
            // 
            this.textBoxDate.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDate.Location = new System.Drawing.Point(23, 38);
            this.textBoxDate.Name = "textBoxDate";
            this.textBoxDate.Size = new System.Drawing.Size(77, 22);
            this.textBoxDate.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(340, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "第二區";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(104, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "第一區";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "開獎日";
            // 
            // tabPageDel
            // 
            this.tabPageDel.Controls.Add(this.buttonDel);
            this.tabPageDel.Controls.Add(this.textBoxDel);
            this.tabPageDel.Controls.Add(this.label2);
            this.tabPageDel.Location = new System.Drawing.Point(4, 25);
            this.tabPageDel.Name = "tabPageDel";
            this.tabPageDel.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDel.Size = new System.Drawing.Size(768, 397);
            this.tabPageDel.TabIndex = 2;
            this.tabPageDel.Text = "刪除";
            this.tabPageDel.UseVisualStyleBackColor = true;
            // 
            // buttonDel
            // 
            this.buttonDel.Location = new System.Drawing.Point(22, 338);
            this.buttonDel.Name = "buttonDel";
            this.buttonDel.Size = new System.Drawing.Size(110, 41);
            this.buttonDel.TabIndex = 15;
            this.buttonDel.Text = "刪除";
            this.buttonDel.UseVisualStyleBackColor = true;
            this.buttonDel.Click += new System.EventHandler(this.ButtonDel_Click);
            // 
            // textBoxDel
            // 
            this.textBoxDel.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDel.Location = new System.Drawing.Point(22, 35);
            this.textBoxDel.Name = "textBoxDel";
            this.textBoxDel.Size = new System.Drawing.Size(77, 22);
            this.textBoxDel.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "開獎日";
            // 
            // tabPagePredict
            // 
            this.tabPagePredict.Controls.Add(this.textBoxPredictSpecialNumber1);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber16);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber15);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber14);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber13);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber12);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber11);
            this.tabPagePredict.Controls.Add(this.labelPredictSpecialNumber1);
            this.tabPagePredict.Controls.Add(this.labelPredictNumber16);
            this.tabPagePredict.Controls.Add(this.labelPredictNumber15);
            this.tabPagePredict.Controls.Add(this.labelPredictNumber14);
            this.tabPagePredict.Controls.Add(this.labelPredictNumber13);
            this.tabPagePredict.Controls.Add(this.labelPredictNumber12);
            this.tabPagePredict.Controls.Add(this.labelPredictNumber11);
            this.tabPagePredict.Controls.Add(this.buttonPredict);
            this.tabPagePredict.Controls.Add(this.textBoxPredictSpecialNumber);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber6);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber5);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber4);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber3);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber2);
            this.tabPagePredict.Controls.Add(this.textBoxPredictNumber1);
            this.tabPagePredict.Controls.Add(this.label5);
            this.tabPagePredict.Controls.Add(this.label6);
            this.tabPagePredict.Location = new System.Drawing.Point(4, 25);
            this.tabPagePredict.Name = "tabPagePredict";
            this.tabPagePredict.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePredict.Size = new System.Drawing.Size(768, 397);
            this.tabPagePredict.TabIndex = 3;
            this.tabPagePredict.Text = "預測";
            this.tabPagePredict.UseVisualStyleBackColor = true;
            // 
            // textBoxPredictSpecialNumber1
            // 
            this.textBoxPredictSpecialNumber1.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxPredictSpecialNumber1.Location = new System.Drawing.Point(17, 303);
            this.textBoxPredictSpecialNumber1.Name = "textBoxPredictSpecialNumber1";
            this.textBoxPredictSpecialNumber1.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictSpecialNumber1.TabIndex = 37;
            // 
            // textBoxPredictNumber16
            // 
            this.textBoxPredictNumber16.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBoxPredictNumber16.Location = new System.Drawing.Point(17, 262);
            this.textBoxPredictNumber16.Name = "textBoxPredictNumber16";
            this.textBoxPredictNumber16.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber16.TabIndex = 36;
            // 
            // textBoxPredictNumber15
            // 
            this.textBoxPredictNumber15.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBoxPredictNumber15.Location = new System.Drawing.Point(17, 223);
            this.textBoxPredictNumber15.Name = "textBoxPredictNumber15";
            this.textBoxPredictNumber15.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber15.TabIndex = 35;
            // 
            // textBoxPredictNumber14
            // 
            this.textBoxPredictNumber14.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBoxPredictNumber14.Location = new System.Drawing.Point(17, 183);
            this.textBoxPredictNumber14.Name = "textBoxPredictNumber14";
            this.textBoxPredictNumber14.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber14.TabIndex = 34;
            // 
            // textBoxPredictNumber13
            // 
            this.textBoxPredictNumber13.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBoxPredictNumber13.Location = new System.Drawing.Point(17, 145);
            this.textBoxPredictNumber13.Name = "textBoxPredictNumber13";
            this.textBoxPredictNumber13.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber13.TabIndex = 33;
            // 
            // textBoxPredictNumber12
            // 
            this.textBoxPredictNumber12.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBoxPredictNumber12.Location = new System.Drawing.Point(17, 101);
            this.textBoxPredictNumber12.Name = "textBoxPredictNumber12";
            this.textBoxPredictNumber12.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber12.TabIndex = 32;
            // 
            // textBoxPredictNumber11
            // 
            this.textBoxPredictNumber11.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBoxPredictNumber11.Location = new System.Drawing.Point(17, 65);
            this.textBoxPredictNumber11.Name = "textBoxPredictNumber11";
            this.textBoxPredictNumber11.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber11.TabIndex = 31;
            // 
            // labelPredictSpecialNumber1
            // 
            this.labelPredictSpecialNumber1.AutoSize = true;
            this.labelPredictSpecialNumber1.BackColor = System.Drawing.Color.LightYellow;
            this.labelPredictSpecialNumber1.Location = new System.Drawing.Point(53, 306);
            this.labelPredictSpecialNumber1.Name = "labelPredictSpecialNumber1";
            this.labelPredictSpecialNumber1.Size = new System.Drawing.Size(124, 17);
            this.labelPredictSpecialNumber1.TabIndex = 30;
            this.labelPredictSpecialNumber1.Text = "03,30,06,11,17,14";
            // 
            // labelPredictNumber16
            // 
            this.labelPredictNumber16.AutoSize = true;
            this.labelPredictNumber16.BackColor = System.Drawing.Color.LightSteelBlue;
            this.labelPredictNumber16.Location = new System.Drawing.Point(53, 263);
            this.labelPredictNumber16.Name = "labelPredictNumber16";
            this.labelPredictNumber16.Size = new System.Drawing.Size(124, 17);
            this.labelPredictNumber16.TabIndex = 29;
            this.labelPredictNumber16.Text = "03,30,06,11,17,14";
            // 
            // labelPredictNumber15
            // 
            this.labelPredictNumber15.AutoSize = true;
            this.labelPredictNumber15.BackColor = System.Drawing.Color.LightSteelBlue;
            this.labelPredictNumber15.Location = new System.Drawing.Point(53, 224);
            this.labelPredictNumber15.Name = "labelPredictNumber15";
            this.labelPredictNumber15.Size = new System.Drawing.Size(124, 17);
            this.labelPredictNumber15.TabIndex = 28;
            this.labelPredictNumber15.Text = "03,30,06,11,17,14";
            // 
            // labelPredictNumber14
            // 
            this.labelPredictNumber14.AutoSize = true;
            this.labelPredictNumber14.BackColor = System.Drawing.Color.LightSteelBlue;
            this.labelPredictNumber14.Location = new System.Drawing.Point(53, 184);
            this.labelPredictNumber14.Name = "labelPredictNumber14";
            this.labelPredictNumber14.Size = new System.Drawing.Size(124, 17);
            this.labelPredictNumber14.TabIndex = 27;
            this.labelPredictNumber14.Text = "03,30,06,11,17,14";
            // 
            // labelPredictNumber13
            // 
            this.labelPredictNumber13.AutoSize = true;
            this.labelPredictNumber13.BackColor = System.Drawing.Color.LightSteelBlue;
            this.labelPredictNumber13.Location = new System.Drawing.Point(53, 145);
            this.labelPredictNumber13.Name = "labelPredictNumber13";
            this.labelPredictNumber13.Size = new System.Drawing.Size(124, 17);
            this.labelPredictNumber13.TabIndex = 26;
            this.labelPredictNumber13.Text = "03,30,06,11,17,14";
            // 
            // labelPredictNumber12
            // 
            this.labelPredictNumber12.AutoSize = true;
            this.labelPredictNumber12.BackColor = System.Drawing.Color.LightSteelBlue;
            this.labelPredictNumber12.Location = new System.Drawing.Point(53, 103);
            this.labelPredictNumber12.Name = "labelPredictNumber12";
            this.labelPredictNumber12.Size = new System.Drawing.Size(124, 17);
            this.labelPredictNumber12.TabIndex = 25;
            this.labelPredictNumber12.Text = "03,30,06,11,17,14";
            // 
            // labelPredictNumber11
            // 
            this.labelPredictNumber11.AutoSize = true;
            this.labelPredictNumber11.BackColor = System.Drawing.Color.LightSteelBlue;
            this.labelPredictNumber11.Location = new System.Drawing.Point(53, 65);
            this.labelPredictNumber11.Name = "labelPredictNumber11";
            this.labelPredictNumber11.Size = new System.Drawing.Size(124, 17);
            this.labelPredictNumber11.TabIndex = 24;
            this.labelPredictNumber11.Text = "03,30,06,11,17,14";
            // 
            // buttonPredict
            // 
            this.buttonPredict.Location = new System.Drawing.Point(21, 334);
            this.buttonPredict.Name = "buttonPredict";
            this.buttonPredict.Size = new System.Drawing.Size(110, 41);
            this.buttonPredict.TabIndex = 23;
            this.buttonPredict.Text = "預測";
            this.buttonPredict.UseVisualStyleBackColor = true;
            this.buttonPredict.Click += new System.EventHandler(this.ButtonPredict_Click);
            // 
            // textBoxPredictSpecialNumber
            // 
            this.textBoxPredictSpecialNumber.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxPredictSpecialNumber.Location = new System.Drawing.Point(257, 34);
            this.textBoxPredictSpecialNumber.Name = "textBoxPredictSpecialNumber";
            this.textBoxPredictSpecialNumber.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictSpecialNumber.TabIndex = 22;
            // 
            // textBoxPredictNumber6
            // 
            this.textBoxPredictNumber6.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxPredictNumber6.Location = new System.Drawing.Point(203, 34);
            this.textBoxPredictNumber6.Name = "textBoxPredictNumber6";
            this.textBoxPredictNumber6.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber6.TabIndex = 21;
            // 
            // textBoxPredictNumber5
            // 
            this.textBoxPredictNumber5.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxPredictNumber5.Location = new System.Drawing.Point(167, 34);
            this.textBoxPredictNumber5.Name = "textBoxPredictNumber5";
            this.textBoxPredictNumber5.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber5.TabIndex = 20;
            // 
            // textBoxPredictNumber4
            // 
            this.textBoxPredictNumber4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxPredictNumber4.Location = new System.Drawing.Point(128, 34);
            this.textBoxPredictNumber4.Name = "textBoxPredictNumber4";
            this.textBoxPredictNumber4.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber4.TabIndex = 19;
            // 
            // textBoxPredictNumber3
            // 
            this.textBoxPredictNumber3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxPredictNumber3.Location = new System.Drawing.Point(92, 34);
            this.textBoxPredictNumber3.Name = "textBoxPredictNumber3";
            this.textBoxPredictNumber3.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber3.TabIndex = 18;
            // 
            // textBoxPredictNumber2
            // 
            this.textBoxPredictNumber2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxPredictNumber2.Location = new System.Drawing.Point(56, 34);
            this.textBoxPredictNumber2.Name = "textBoxPredictNumber2";
            this.textBoxPredictNumber2.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber2.TabIndex = 17;
            // 
            // textBoxPredictNumber1
            // 
            this.textBoxPredictNumber1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBoxPredictNumber1.Location = new System.Drawing.Point(20, 34);
            this.textBoxPredictNumber1.Name = "textBoxPredictNumber1";
            this.textBoxPredictNumber1.Size = new System.Drawing.Size(30, 22);
            this.textBoxPredictNumber1.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(254, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "第二區";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "第一區";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControlMain);
            this.Name = "FormMain";
            this.Text = "樂透";
            this.tabControlMain.ResumeLayout(false);
            this.tabPageFind.ResumeLayout(false);
            this.tabPageFind.PerformLayout();
            this.tabPageAdd.ResumeLayout(false);
            this.tabPageAdd.PerformLayout();
            this.tabPageDel.ResumeLayout(false);
            this.tabPageDel.PerformLayout();
            this.tabPagePredict.ResumeLayout(false);
            this.tabPagePredict.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageFind;
        private System.Windows.Forms.TabPage tabPageAdd;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelSpecialNumberInfo;
        private System.Windows.Forms.Label labelNumbersInfo;
        private System.Windows.Forms.Label labelDateInfo;
        private System.Windows.Forms.Label labelSpecialNumber;
        private System.Windows.Forms.Label labelNumbers;
        private System.Windows.Forms.TextBox textBoxDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.TextBox textBoxSpecialNumber;
        private System.Windows.Forms.TextBox textBoxNumber6;
        private System.Windows.Forms.TextBox textBoxNumber5;
        private System.Windows.Forms.TextBox textBoxNumber4;
        private System.Windows.Forms.TextBox textBoxNumber3;
        private System.Windows.Forms.TextBox textBoxNumber2;
        private System.Windows.Forms.TextBox textBoxNumber1;
        private System.Windows.Forms.TabPage tabPageDel;
        private System.Windows.Forms.Button buttonDel;
        private System.Windows.Forms.TextBox textBoxDel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPagePredict;
        private System.Windows.Forms.TextBox textBoxPredictSpecialNumber;
        private System.Windows.Forms.TextBox textBoxPredictNumber6;
        private System.Windows.Forms.TextBox textBoxPredictNumber5;
        private System.Windows.Forms.TextBox textBoxPredictNumber4;
        private System.Windows.Forms.TextBox textBoxPredictNumber3;
        private System.Windows.Forms.TextBox textBoxPredictNumber2;
        private System.Windows.Forms.TextBox textBoxPredictNumber1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonPredict;
        private System.Windows.Forms.TextBox textBoxPredictSpecialNumber1;
        private System.Windows.Forms.TextBox textBoxPredictNumber16;
        private System.Windows.Forms.TextBox textBoxPredictNumber15;
        private System.Windows.Forms.TextBox textBoxPredictNumber14;
        private System.Windows.Forms.TextBox textBoxPredictNumber13;
        private System.Windows.Forms.TextBox textBoxPredictNumber12;
        private System.Windows.Forms.TextBox textBoxPredictNumber11;
        private System.Windows.Forms.Label labelPredictSpecialNumber1;
        private System.Windows.Forms.Label labelPredictNumber16;
        private System.Windows.Forms.Label labelPredictNumber15;
        private System.Windows.Forms.Label labelPredictNumber14;
        private System.Windows.Forms.Label labelPredictNumber13;
        private System.Windows.Forms.Label labelPredictNumber12;
        private System.Windows.Forms.Label labelPredictNumber11;
    }
}

