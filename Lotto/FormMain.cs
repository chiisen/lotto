﻿using NLog;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Lotto
{
    public partial class FormMain : Form
    {
        private List<LottoData> list = new List<LottoData>();

        private Logger log = LogManager.GetCurrentClassLogger();

        public FormMain()
        {
            InitializeComponent();

            ReadData();

            // 指定頁面
            this.tabControlMain.SelectedTab = tabPagePredict;

            // 清除資料
            // 清除預測資料
            labelPredictNumber11.Text = "無";
            labelPredictNumber12.Text = "無";
            labelPredictNumber13.Text = "無";
            labelPredictNumber14.Text = "無";
            labelPredictNumber15.Text = "無";
            labelPredictNumber16.Text = "無";
            labelPredictSpecialNumber1.Text = "無";
        }

        public void ReadData()
        {
            //! 讀取資料
            Sql sql_ = new Sql();
            var dataTable = sql_.GetDataTable(@"SELECT * FROM Lotto");

            labelDateInfo.Text = "";
            labelNumbersInfo.Text = "";
            labelSpecialNumberInfo.Text = "";

            
            //! 清除 list
            list.Clear();

            foreach (DataRow dataRow in dataTable.Rows)
            {
                string date_ = dataRow.ItemArray[0].ToString();

                LottoData lottoData_ = new LottoData(date_, dataRow.ItemArray);
                list.Add(lottoData_);
            }


            //! 依 list 做遞增排序
            // ascending 小到大
            // descending 大到小
            var queryOrder = from e in list
                             orderby e.lDate descending
                             select e;

            foreach (var e in queryOrder)
            {
                //! 顯示在介面上
                labelDateInfo.Text += e.TaiwanDate;
                labelDateInfo.Text += "\n";

                log.Debug(e.TaiwanDate);

                string numbersLine_ = e.NumbersLine();
                log.Debug(numbersLine_);
                labelNumbersInfo.Text += numbersLine_;
                labelNumbersInfo.Text += "\n";

                string specialNumber_ = e.SpecialNumber.ToString("D2");
                log.Debug(specialNumber_);
                labelSpecialNumberInfo.Text += specialNumber_;
                labelSpecialNumberInfo.Text += "\n";

                log.Debug("=======");
            }
        }

        /// <summary>
        //? 新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonAdd_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxDate.Text) ||
                string.IsNullOrEmpty(textBoxNumber1.Text) ||
                string.IsNullOrEmpty(textBoxNumber2.Text) ||
                string.IsNullOrEmpty(textBoxNumber3.Text) ||
                string.IsNullOrEmpty(textBoxNumber4.Text) ||
                string.IsNullOrEmpty(textBoxNumber5.Text) ||
                string.IsNullOrEmpty(textBoxNumber6.Text) ||
                string.IsNullOrEmpty(textBoxSpecialNumber.Text))
            {
                MessageBox.Show("新增資料未輸入", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Sql sql_ = new Sql();

            string sqlCmd_ = string.Format($@"INSERT INTO Lotto(Date, 
                                                                Number1, 
                                                                Number2, 
                                                                Number3, 
                                                                Number4, 
                                                                Number5, 
                                                                Number6, 
                                                                SpecialNumber) 
                                              VALUES('{textBoxDate.Text}', 
                                                       {textBoxNumber1.Text}, 
                                                       {textBoxNumber2.Text},
                                                       {textBoxNumber3.Text}, 
                                                       {textBoxNumber4.Text},
                                                       {textBoxNumber5.Text},
                                                       {textBoxNumber6.Text},
                                                       {textBoxSpecialNumber.Text})");
            log.Debug(sqlCmd_);
            sql_.Execute(sqlCmd_);

            ReadData();

            // 清除輸入資料
            textBoxDate.Text = "";
            textBoxNumber1.Text = "";
            textBoxNumber2.Text = "";
            textBoxNumber3.Text = "";
            textBoxNumber4.Text = "";
            textBoxNumber5.Text = "";
            textBoxNumber6.Text = "";
            textBoxSpecialNumber.Text = "";


            MessageBox.Show("新增成功", "OK", MessageBoxButtons.OK, MessageBoxIcon.Question);

        }

        /// <summary>
        //? 刪除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonDel_Click(object sender, System.EventArgs e)
        {
            if(string.IsNullOrEmpty(textBoxDel.Text))
            {
                MessageBox.Show("刪除日期未輸入", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Sql sql_ = new Sql();

            string sqlCmd_ = string.Format($@"DELETE FROM Lotto WHERE Date = '{textBoxDel.Text}'");
            log.Debug(sqlCmd_);
            sql_.Execute(sqlCmd_);

            
            ReadData();

            // 清除輸入資料
            textBoxDel.Text = "";


            MessageBox.Show("刪除成功", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        //? 預測
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPredict_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxPredictNumber1.Text) ||
                string.IsNullOrEmpty(textBoxPredictNumber2.Text) ||
                string.IsNullOrEmpty(textBoxPredictNumber3.Text) ||
                string.IsNullOrEmpty(textBoxPredictNumber4.Text) ||
                string.IsNullOrEmpty(textBoxPredictNumber5.Text) ||
                string.IsNullOrEmpty(textBoxPredictNumber6.Text) ||
                string.IsNullOrEmpty(textBoxPredictSpecialNumber.Text))
            {
                MessageBox.Show("預測資料未輸入", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //! 要預測的資料
            int number1_ = int.Parse(textBoxPredictNumber1.Text);
            int number2_ = int.Parse(textBoxPredictNumber2.Text);
            int number3_ = int.Parse(textBoxPredictNumber3.Text);
            int number4_ = int.Parse(textBoxPredictNumber4.Text);
            int number5_ = int.Parse(textBoxPredictNumber5.Text);
            int number6_ = int.Parse(textBoxPredictNumber6.Text);
            int numberS_ = int.Parse(textBoxPredictSpecialNumber.Text);

            textBoxPredictNumber11.Text = number1_.ToString("D2");
            textBoxPredictNumber12.Text = number2_.ToString("D2");
            textBoxPredictNumber13.Text = number3_.ToString("D2");
            textBoxPredictNumber14.Text = number4_.ToString("D2");
            textBoxPredictNumber15.Text = number5_.ToString("D2");
            textBoxPredictNumber16.Text = number6_.ToString("D2");
            textBoxPredictSpecialNumber1.Text = numberS_.ToString("D2");

            //! 清除輸入資料
            textBoxPredictNumber1.Text = "";
            textBoxPredictNumber2.Text = "";
            textBoxPredictNumber3.Text = "";
            textBoxPredictNumber4.Text = "";
            textBoxPredictNumber5.Text = "";
            textBoxPredictNumber6.Text = "";
            textBoxPredictSpecialNumber.Text = "";

            //! 統計
            LottoCount num1_ = new LottoCount(number1_);
            LottoCount num2_ = new LottoCount(number2_);
            LottoCount num3_ = new LottoCount(number3_);
            LottoCount num4_ = new LottoCount(number4_);
            LottoCount num5_ = new LottoCount(number5_);
            LottoCount num6_ = new LottoCount(number6_);
            LottoCount numS_ = new LottoCount(numberS_);

            List<LottoCount> foreachNumbers_ = new List<LottoCount>()
            {
                num1_,
                num2_,
                num3_,
                num4_,
                num5_,
                num6_,
                numS_
            };

            //! 依 list 做'日期'的遞增排序 -> 近到遠
            // ascending 小到大
            // descending 大到小
            var queryOrder = from n in list
                             orderby n.lDate descending
                             select n;

            foreach (var n in queryOrder)
            {
                log.Debug(string.Format("日期(近到遠): {0}", n.TaiwanDate));

                foreach(var oneNunber in foreachNumbers_)
                {
                    oneNunber.Check(n);
                }
            }


            //TODO: 更新預測
            //x 先填預設值，之後要實作。
            labelPredictNumber11.Text = num1_.NumbersLine();
            labelPredictNumber12.Text = num2_.NumbersLine();
            labelPredictNumber13.Text = num3_.NumbersLine();
            labelPredictNumber14.Text = num4_.NumbersLine();
            labelPredictNumber15.Text = num5_.NumbersLine();
            labelPredictNumber16.Text = num6_.NumbersLine();
            labelPredictSpecialNumber1.Text = numS_.NumbersLine();


            MessageBox.Show("預測成功", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
