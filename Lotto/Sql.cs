﻿using System;
using System.Data;
using System.Data.SQLite;

namespace Lotto
{
    public class Sql
    {
        public string database = "lotto.db";// 資料庫名稱

        /// <summary>讀取資料</summary>
        /// <param name="sqlQuery">資料查詢的 SQL 語句</param>
        /// <returns></returns>
        public DataTable GetDataTable(string sqlQuery)
        {
            var connection = OpenConnection();
            var dataAdapter = new SQLiteDataAdapter(sqlQuery, connection);
            var myDataTable = new DataTable();
            var myDataSet = new DataSet();
            myDataSet.Clear();
            dataAdapter.Fill(myDataSet);
            myDataTable = myDataSet.Tables[0];
            if (connection.State == ConnectionState.Open) connection.Close();
            return myDataTable;
        }

        /// <summary>建立資料庫連線</summary>
        /// <returns></returns>
        private SQLiteConnection OpenConnection()
        {
            var conntion = new SQLiteConnection()
            {
                ConnectionString = $"Data Source={database};Version=3;New=False;Compress=True;"
            };
            if (conntion.State == ConnectionState.Open) conntion.Close();
            conntion.Open();
            return conntion;
        }

        /// <summary>新增\修改\刪除資料</summary>
        /// <param name="sqlExecute">資料操作的 SQL 語句</param>
        public void Execute(string sqlExecute)
        {
            var connection = OpenConnection();
            var command = new SQLiteCommand(sqlExecute, connection);
            var mySqlTransaction = connection.BeginTransaction();
            try
            {
                command.Transaction = mySqlTransaction;
                command.ExecuteNonQuery();
                mySqlTransaction.Commit();
            }
            catch (Exception ex)
            {
                mySqlTransaction.Rollback();
                throw (ex);
            }
            if (connection.State == ConnectionState.Open) connection.Close();
        }
    }
}
